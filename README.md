
# SujIUT - Vos annales de BUT ici

SujIUT est une plateforme de dépôt des examens passés sur les différentes ressources des différents BUT. À l'heure actuelle, seul le BUT Réseaux et Télécommunications est géré.




## Authors

- Hugo Clamond
- Axel Mourillon



## Contribuer

Les contributions sont toujours les bienvenues !

Veuillez lire `contributing.md` pour commencer à contribuer.

Merci de respecter le `code de conduite` du projet.

