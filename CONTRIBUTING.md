
# Contribuez au projet SujIUT

Tout d'abord, merci de contribuer à la banque de sujets SujIUT ! Vous aidez par ce moyen les prochaines promos dans leurs passages d'examens.

## Ajouter de nouveaux sujets / corrigés

Cette partie va dérouler le processus de contribution, dans le but d'envoyer de nouveaux sujets.

1. Forkez le repo. Pour ce faire, vous devez vous rendre sur la page du repo et cliquer sur le bouton Fork, sur la partie droite de l'écran. 

2. Ajoutez vos sujets. Une convention de nommage doit être respectée qui est la suivante : `<initiales_du_BUT><année_de_rentrée>_<ressource_ou_SAE>_<type_d'examen>.pdf`

Par exemple, si une contribution doit ajouter un DS Bonus de la ressource R101 du BUT Réseaux & Télécommunications de l'année 2022, alors le sujet sera nommé comme suit :
`RT1_2022_R101_DS_Bonus.pdf`

Si une contribution doit ajouter la version corrigée de ce même sujet, elle devra être nommée comme suit :
`RT1_2022_R101_DS_Bonus_corrected.pdf`

**Attention** : Votre sujet doit être au format PDF. Il est recommendé de le scanner si vous ne possédez pas de version numérique.

3. Envoyez vos modifications sur votre repo forké avec les commandes git classiques :
`git add`, `git commit`, `git push`.

4. Créez une Merge Request. Vous sélectionnerez en `source` votre repo avec la branche main, et en `target` le repo d'origine (sujiut) avec la branche main.

## Contribuer au développement du projet

Si vous souhaitez améliorer le projet SujIUT, voici les règles à respecter pour que l'on travaille dans de bonnes conditions. Cela vaut autant pour les contributeurs externes qu'à l'équipe de développement interne.

Le workflow de ce projet est basé sur un workflow Git nommé __Feature Branch Workflow__. Pour chaque nouvelle fonctionnalité, on crée une nouvelle branche. Lorsque la fonctionnalité est prête à être déployée en production, on fusionne la branche de fonctionnalité avec la branche `main`. Pour les contributeurs externes, il faudra passer par une `Merge Request`. Ceux-ci doivent travailler sur un Fork du dépôt.

Il est important de consulter régulièrement cette section de ce fichier étant donné que le workflow est amené à évoluer. À ce stade du projet, il est encore difficile de savoir comment procéder.

### Et c'est tout !

Si votre contribution convient aux normes décrites ci-dessus, elle sera intégrée au projet et sera disponible la consultation sur le site de SujIUT. Merci !



