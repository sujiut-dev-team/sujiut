
# Code de conduite

Afin que votre contribution soit acceptée sans accros et pour faciliter le travail des membres du projet, un code de conduite doit être respecté.

## Ne clonez pas le repo, forkez le

Forker un repo permet d'effectuer des modifications sans risquer d'affecter le repo d'origine, ce qui rend le projet public plus stable.

Réferez vous au fichier `CONTRIBUTING.md` pour plus d'informations.

## Respectez les règles de nommage

Ces règles permettent de faciliter le tri des fichiers PDF dans le cas où ils ne seraient pas dans le bon dossier. Ce qui nous amène au point suivant.

## Placez les sujets dans les bons dossiers.

Pour retrouver plus facilement les sujets sur le site, mettez les dans les bons dossiers. Par exemple, si vous avez un sujet de première année en R&T, cela se passera dans `RT1`. 

Si un dossier de ressource ou de SAE n'existe pas, créez le. Si vous avez vu qu'il existe ailleurs mais pas dans le bon dossier parent, déplacez le.

**Merci d'avoir pris le temps de lire ces règles de conduite.**

